import React from 'react';
import ai from 'images/ai.gif';
import styles from './styles.scss';

const Test = () => (
    <div>
        <h1 className={styles.test}>Hello there World!</h1>
        <img src={ai} alt="gif" />
    </div>
);

export default Test;
