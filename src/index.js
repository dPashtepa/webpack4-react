import React from 'react';
import ReactDOM from 'react-dom';
import Test from 'components/test';

import 'styles/index.scss';

const HelloWorld = () => <Test />;

ReactDOM.render(
    <HelloWorld />,
    document.getElementById('root')
);

module.hot.accept();
